<?php
// Defining autoloader
spl_autoload_register(function ($class_name) {
	if(file_exists('classes/'.$class_name . '.php')) {
        require_once( 'classes/'.$class_name.'.php' );
    } else {
        throw new Exception("Unable to load $class_name.");
    }
});