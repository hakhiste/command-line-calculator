# Command Line Calculator

We have to make a simple command line calculator

# Task 1
We need to make a command line utility to sum zero to two numbers. Command
should be:
php calculator.php sum
php calculator.php sum 1
php calculator.php sum 2,3
Expected outputs of above commands are 0, 1, and 5 respectively.

# Task 2
Now we need to add capability of handling multiple numbers so that all following commands work correctly.

# Task 3
Allow add method to use new line character \n as number separator. 
Example
php calculator.php add 2\n3,4
is valid and will return 9.

# Task 4
Support different delimiters. Add method should allow defining what delimiter is used to separate numbers.
Format \\[delimiter]\\numbers. To make it easy, \ will never be a part of delimiter.

# Task 5
Negative numbers should show error.

# Task 6
Also show the negative numbers in error message

# Task 7
Ignore numbers above 1000.

# Task 8
Making multiply functionality.