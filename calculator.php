<?php
require __DIR__ . '/autoload.php';

try {
    $objCalculator = new Calculator();

    //parse input arguments
    list($command, $parameters) = $objCalculator->parseArguments($argv);
    
    //filter parameters & get valid array of values
    $arrParameter = $objCalculator->filterParameters($parameters);
    
    //call command function
    echo $objCalculator->doOperation($command, $arrParameter);
} catch (Exception $e) {
    echo 'Error: '.$e->getMessage();
}
