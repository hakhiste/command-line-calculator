<?php

/**
* Calculator class
*/
class Calculator
{
    /**
     * Check all arguments passed are valid or not
     *
     * @param string $argv arguments.
     * @return array|exception return array(command, paramers) if valid arguments; otherwise throw exception
     */
    public function parseArguments($argv)
    {
        $parameters = null;
        $seperator  = ',';

        // if arguments are not empty
        if (isset($argv) && !empty($argv)) {
            list($fileName, $command) = $argv;
        
            // if parameter passed as argument
            if (isset($argv[2])) {
                $parameters = $argv[2];
            }
            
            return array($command, $parameters);
        } else {
            throw new Exception('Oops! Parsing arguments fail.');
        }
    }

    /**
     * Returns output of input.
     *
     * @param int|bool $input Input as integer or boolean value.
     * @return int|bool Output pretty much the same.
     */
    public function filterParameters($parameters)
    {
        $seperator = ',';

        /**
         * Task 3
         * Allow add method to use new line character \n as number separator.
         */
        $parameters = str_replace(array('\n', 'n'), $seperator, $parameters);

        /**
         * Task 4
         * Support different delimiters.
         * Format \\[delimiter]\\numbers.
         */
        if (strpos($parameters, "\\") !== false) {
            $parameters = str_replace('\\', '', $parameters);
            $seperator = substr($parameters, 0, 1);
            
            $pattern = '/[\'\/~\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';

            // if invalid seperator is passed then throw exception
            if (!preg_match($pattern, $seperator)) {
                throw new Exception('Invalid serperator');
            }

            $arrParameter = explode($seperator, $parameters);
            unset($arrParameter[0]);
        } else {
            /**
             * Task 1
             * We need to make a command line utility to sum zero to two numbers.
             */
            $arrParameter = explode($seperator, $parameters);
        }

        $arrParameter = $this->checkValidNumbers($arrParameter);

        return $arrParameter;
    }

    /**
     * check is parameters are valid numbers and as per rules
     *
     * @param array $arrParameter array of parameters
     * @return array|exception return array of valid parameters. Throw expcetion if invalid value provided
     */
    public function checkValidNumbers($arrParameter)
    {
        /**
         * Task 5
         * Negative numbers should show error.
         */
        $arrNegative = [];
        array_walk($arrParameter, function ($value, $key) use (&$arrNegative) {
            if (empty($value)) {
                return 0;
            }
            if (!is_numeric($value)) {
                throw new Exception("Invalid value or seperator passed: ".$value);
            }
            if ($value<=0) {
                $arrNegative[] = $value;
            }
        });

        /*
         * Task 6
         * Also show the negative numbers in error message
         */
        if (!empty($arrNegative)) {
            throw new Exception("Negative numbers (".implode(', ', $arrNegative).") not allowed.");
        }

        /**
         * Task 7
         * Ignore numbers above 1000. For example, command
         */
        $arrParameter = array_filter($arrParameter, function ($v) {
            return $v < 1000;
        });

        return $arrParameter;
    }

    /**
     * Do command operation
     *
     * @param string $command action command
     * @param array $arrParameter array of parameters
     * @return int result
     */
    public function doOperation($command, $arrParameter)
    {
        switch ($command) {
            case 'add': case 'sum':
                return $this->doAddition($arrParameter);
                break;
            case 'multiply':
                return $this->doMultiply($arrParameter);
                break;
            default:
                throw new Exception("Invalid command in argument.");
                break;
        }
    }

    /**
     * Addition of array
     *
     * @param array $arrParameter array of parameters
     * @return int array sum
     */
    public function doAddition($arrParameter) {
        return array_sum($arrParameter);
    }

    /**
     * Multiplication of array
     *
     * @param array $arrParameter array of parameters
     * @return int array multiplication
     */
    public function doMultiply($arrParameter) {
        return array_product($arrParameter);
    }
}
